<?php require_once "./code.php"?>
<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>S04: Access Modifiers and Encapsulation</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel='stylesheet' type='text/css' media='screen' href='main.css'>
    <script src='main.js'></script>
</head>
<body>
    <!-- <h1>Access Modifiers</h1>
    <h3>Buidling Variables</h3>
    
    <h3>Mini Activity</h3>
    <p>The building has <?//= $bldg->getFloors() ?> floors</p>
    
    <p>The building has <?//= $condo->getFloors() ?> floors</p>
    <h3>Condominium Variables</h3>
    
    <h1>Encapsulation</h1>
    <p>The name of the condominium is <?//= $condo->getName()?></p>
    <?php 
        //$condo->setName('Enzo Tower')
    ?>
    <p>The name of the condominium has been changed to <?//= $condo->getName()?></p> -->

    <h1>Activity</h1>
    <h3>Building</h3>
    <p>The name of the building is <?= $bldg->getName()?></p>
    <p>The <?= $bldg->getName()?> has <?= $bldg->getFloors()?> floors</p>
    
    <p>The <?= $bldg->getName()?> has <?= $bldg->getFloors()?> floors</p>
    <p>The <?= $bldg->getName()?> is located at <?= $bldg->getAddress()?> </p>
    <?php $bldg->setName('Caswyn Complex'); ?>
    <p>The name of the building has been changed to <?= $bldg->getName();?></p>
    <h3>Condominium</h3>
    <p>The name of the building is <?= $condo->getName()?></p>
    <p>The <?= $condo->getName()?> has <?= $condo->getFloors()?> floors</p>
    <p>The <?= $condo->getName()?> is located at <?= $condo->getAddress()?> </p>
    <?php $condo->setAddress('Cebu City')?>
    <p>The <?= $condo->getName()?> is located at <?= $condo->getAddress()?> </p>
    <?php $condo->setName('Enzo Tower'); ?>
    <p>The name of the building has been changed to <?= $condo->getName();?></p>
</body>
</html>
